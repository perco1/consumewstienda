






{ TABLE "informix".pedido_e row size = 638 number of columns = 18 index size = 9 }

create table "informix".pedido_e 
  (
    num_pedido integer,
    fecha datetime year to second,
    nombre char(70),
    casa_trj smallint,
    autorizacion char(25),
    nit_factura char(12),
    nombre_factura char(70),
    nombre_recibe char(70),
    direccion char(70),
    departamento char(20),
    municipio char(20),
    direccion_entrega char(70),
    telefono char(20),
    e_mail char(50),
    comentario char(100),
    total_doc decimal(10,2),
    estado_orden char(20),
    estatus char(1)
  );

revoke all on "informix".pedido_e from "public" as "informix";


create unique index "informix".ixped_e1 on "informix".pedido_e 
    (num_pedido) using btree ;
alter table "informix".pedido_e add constraint primary key (num_pedido) 
    constraint "modula".pk_pedido_e  ;


